#!/bin/sh

  docker run -e HADOLINT_NOFAIL=1 \
  --interactive --rm \
  -i hadolint/hadolint < Dockerfiles/$1/Dockerfile