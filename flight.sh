#!/bin/sh
# Name of the file to search for the variable
file=".gitlab-ci.yml"
# Name of the variable to change its value
variable="CI_CHANGE"

# Prompt the user to enter the new boolean value of the variable
read -p "Enter the new boolean value of the variable $variable (true or false): " new_value

# Check that the user entered a valid boolean value
if [[ "$new_value" != "true" && "$new_value" != "false" ]]; then
  echo "Invalid boolean value entered. Please enter 'true' or 'false'."
  exit 1
fi

# Find the variable in the file and change its value
if grep -q "$variable:" "$file"; then
  sed -i '' -e "s/$variable\:.*/$variable: \"$new_value\"/" "$file"
  echo "The boolean variable $variable has been changed to $new_value in the file $file."
else
  echo "The variable $variable was not found in the file $file."
fi