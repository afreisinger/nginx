# !bin/bash

# NAMESPACE is the namespace to deploy to, and is also set in .gitlab-ci.yml
# CI_COMMIT_REF_SLUGis the name of the branch
# CI_BUILD_REF is the SHA of the commit

if [ "$1" == "" ]; then
  echo "local-runner.sh requires the job-name as argument"
  exit 1
fi  

source set_enviroment.sh
#source set-runner-env.sh
env | egrep 'CI|DOCKER' | sort

#cd .. && gitlab-runner exec docker \
cd .. && gitlab-runner exec docker \
    --env CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG} \
    --env CI_PIPELINE_ID=${CI_PIPELINE_ID} \
    --env CI_PROJECT_NAME=${CI_PROJECT_NAME} \
    --env CI_REGISTRY=${CI_REGISTRY} \
    --env CI_REGISTRY_USER=${CI_REGISTRY_USER} \
    --env CI_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD} \
    --env DOCKER_REGISTRY=${DOCKER_REGISTRY} \
    --env DOCKER_REGISTRY_USER=${DOCKER_REGISTRY_USER} \
    --env DOCKER_REGISTRY_PASSWORD=${DOCKER_REGISTRY_PASSWORD} \
"$1"

    