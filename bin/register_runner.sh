# !bin/bash
# https://docs.gitlab.com/runner/register/
source set_enviroment.sh #secrets

RUNNER_NAME=macOS shared runner
URL=https://gitlab.com
TOKEN=${GITLAB_RUNNER_TOKEN} #.env

#AWS_ACCESS_KEY=
#AWS_SECRET_ACCESS_KEY=
#BUCKET_NAME=


gitlab-runner register  --non-interactive \
  --config "${HOME}/.gitlab-runner/config.toml" \
  --name ${RUNNER_NAME} \
  --url ${URL} \
  --registration-token ${TOKEN} \
  --executor docker \
  --docker-image docker:latest \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "docker, shared-macos-amd64" \
  --docker-pull-policy if-not-present \
  --locked=false \
  --run-untagged=true \
  --docker-privileged=true \
  --docker-volumes=["/certs/client", "/var/run/docker.sock:/var/run/docker.sock", "/cache"] \
  --access-level="not_protected"
  
  #--cache-s3-server-address s3.amazonaws.com \
  #--cache-type s3 --cache-s3-access-key ${AWS_ACCESS_KEY} --cache-s3-secret-key ${AWS_SECRET_ACCESS_KEY} \
  #--cache-s3-bucket-name ${BUCKET_NAME} --cache-s3-bucket-location eu-west-2 --cache-s3-insecure false