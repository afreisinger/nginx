#!/bin/sh
#Build NGINX  Docker container
#

# Usage EXAMPLE: ./build-nginx.sh ubuntu18.04
distro="$(tr [A-Z] [a-z] <<< "$1")" # set to lowercase
echo $distro
    # Set build directory
    build_dir='./build'
    
    if [ -d "$build_dir" ]
    then
        # remove Dockerfile here (if exists)
        rm -rf $build_dir/* || true
    else
        mkdir "$build_dir"
    fi 
     

    # copy desired Dockerfile
    cp Dockerfiles/$distro/* $build_dir
    cp -R etc/ $build_dir/etc
    
    # Build and tag it as "nginx-plus-[distro]"
    docker build -t $distro.local  $build_dir --pull --no-cache # No caching
    
    # Show all docker containers build with names containing "nginx-"
    printf "\n"
    printf "Containers built:"
    printf "\n"
    docker images | grep .local

    # remove Dockerfile from the build directory (if exists)
    rm -rf $build_dir/* || true