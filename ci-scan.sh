#!/bin/sh

docker run \
  --interactive --rm \
  --volume "$PWD":/tmp/app \
  -e CI_PROJECT_DIR=/tmp/app \
  -e CI_APPLICATION_REPOSITORY=$CI_REGISTRY/$CI_PROJECT_PATH \
  -e CI_APPLICATION_TAG=$1 \
  registry.gitlab.com/security-products/container-scanning